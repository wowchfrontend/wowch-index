$(document).ready(function(){
	$('.welcome-datepicker').datepicker({
		format: 'dd/mm/yyyy',
		startDate: '+0d',
		autoclose: true
	});
	$('.welcome-timepicker').timepicker();

	if($('.timer').length){
		$('.timer').startTimer({
			onComplete: function(element){
				element.addClass('is-complete');
				$('.quiz-button').prop('disabled', true);
				//TIMER ON COMPLETE CALLBACK
				window.location = "index.html"; // change this location to page you need
			}
		});
	}

//RADIO VALIDATION ON QUIZ PAGE
if($('.quiz-radio').length){
	function checkedRadios (){
		$('.quiz-radio').find("input[type='radio']").each(function(){
			if($(this).is(':checked')){
				$('.quiz-button').prop('disabled', false);
				return false;
			} else {
				$('.quiz-button').prop('disabled', true);
			}
		})
	}
	checkedRadios();
	$('.quiz-radio').on('click', function(){
		checkedRadios();
	})
}

//TOOLTIP SHOW/HIDE on WELCOME PAGE
$(".welcome-form").hover( function () {
    $(this).find('.tooltip-block').stop().fadeIn();
  },
  function () {
    $(this).find('.tooltip-block').stop().fadeOut();
  }
);


$('.scrollTo').click(function(){
  $('html, body').animate({
      scrollTop: $( $.attr(this, 'href') ).offset().top
  }, 500);
  return false;
});

//FORM VALIDATION

$('.submit_welcome').on('click', function(){
	var erorrsExists = false;
	$('.welcome_validate').each(function(){
		if($(this).val().replace(/^\s+/g, '') == '' || $(this).val() == 'Date'){
			$(this).parent().find('.field_error').addClass('show');
			erorrsExists = true;
		} else {
			$(this).parent().find('.field_error').removeClass('show');
		}
	});
	if(erorrsExists){return false;}
});

})